const addToCart = (allProducts, setAllProducts, index, setCartVisibility) => {
  const nextState = [...allProducts]
  const product = nextState[index]

  if (product.addedToCart) {
    nextState[index] = {...product, quantity: product.quantity + 1}
  } else {
    nextState[index] = {...product, quantity: 1, index}
  }

  nextState[index] = {...nextState[index], addedToCart: true}

  setAllProducts(nextState)
  setCartVisibility(true)
}

const decrementCartItem = (allProducts, setAllProducts, index) => {
  const nextState = [...allProducts]
  const product = nextState[index]

  if(product.quantity > 1) {
    nextState[index] = {...product, quantity: product.quantity - 1}
  }
  else {
    nextState[index] = {...product, quantity: 0, addedToCart: false}
  }

  setAllProducts(nextState);
}

const removeFromCart = (allProducts, setAllProducts, index ) => {
  const nextState = [...allProducts]
  nextState[index] = {...nextState[index], quantity: 0, addedToCart: false}
  setAllProducts(nextState)
}

export { addToCart, decrementCartItem, removeFromCart }
