import getSymbolFromCurrency from 'currency-symbol-map'
import './cart.scss'

const Cart = ({
  addToCart,
  allProducts,
  setAllProducts,
  decrementCartItem,
  removeFromCart,
  setCartVisibility,
  currencyData,
  currency,
  setCurrency,
  refetchProductData
}) => {

  const cartItems = allProducts?.filter(product => product.addedToCart === true)
  let subtotal = 0
  cartItems?.forEach(item => {
    subtotal += item.quantity * item.price
  })

  return (
    <div className='cart'>
      <input
        type='button'
        className='button back'
        value='<'
        onClick={() => setCartVisibility(false)}
      />
      <select id="currencies" onChange={e => {
        refetchProductData({ currency: e.target.value })
        setCurrency(e.target.value)
      }}>
        {currencyData?.currency.map(currency => (
          <option value={currency} key={currency}>{currency}</option>
        ))}
      </select>
      {cartItems?.map(product => (
        product && <div className='item' key={product.id}>
          <div className='first-row'>
            <p className='title'>{product.title}</p>
            <input
              className='button cancel'
              type='button'
              onClick={() => removeFromCart(allProducts, setAllProducts, product.index)}
              value='X'
            />
          </div>
          <div className='second-row'>
            <div className='quantity'>
              <input
                className='button decrement'
                type='button'
                onClick={() => decrementCartItem(allProducts, setAllProducts, product.index)}
                value='-'
              />

              <p className='value'>{product.quantity}</p>

              <input
                type='button'
                className='button increment'
                onClick={() => addToCart(
                  allProducts, setAllProducts, product.index, setCartVisibility
                )}
                value='+'
              />
            </div>
            <p className='price'>{getSymbolFromCurrency(currency)}{product.price.toFixed(2)}</p>
            <div className='product-img'>
              <img alt='product' className='product-img' src={product.image_url} />
            </div>
          </div>
        </div>
      ))}
      <div className='summary'>
        <p>Subtotal</p>
        <p>{getSymbolFromCurrency(currency)} {subtotal.toFixed(2)}</p>
      </div>
      <div>
        <input
          type='button'
          className='button subscription'
          value='MAKE THIS A SUBSCRIPTION (SAVE 20%)'
        />
        <input
          type='button'
          className='button checkout'
          value='PROCEED TO CHECKOUT'
        />
      </div>
    </div>
  )
}

export { Cart }
