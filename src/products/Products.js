import getSymbolFromCurrency from 'currency-symbol-map'
import './products.scss'

const Products = ({
  allProducts,
  setAllProducts,
  addToCart,
  setCartVisibility,
  currency
}) => {
  return (
    <div className='products'>
      <div className='heading'>
        <p>All Products</p>
        <span>A 360° look at Lumin</span>
      </div>
      <div className= 'showcase'>
        {allProducts?.map((product, index) => (
          <div className='item' key={product.id}>
            <img alt='product' src={product.image_url} />
            <p className='title'>{product.title}</p>
            <p className='price'>From {getSymbolFromCurrency(currency)}{product.price.toFixed(2)}</p>
            <button type='button' onClick={() => addToCart(
              allProducts, setAllProducts, index, setCartVisibility
            )}>
              Add to Cart
            </button>
          </div>
        ))}
      </div>
    </div>
  )
}

export { Products }
