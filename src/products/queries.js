import { gql } from '@apollo/client'

const GET_ALL_PRODUCTS = gql`
  query Products($currency: Currency) {
    products {
      id
      title
      image_url
      price(currency: $currency)
    }
  }
`

const GET_ALL_CURRENCIES = gql`
  query Currency {
    currency
  }
`

export { GET_ALL_PRODUCTS, GET_ALL_CURRENCIES }
