import { useState, useEffect } from 'react'
import { useQuery } from '@apollo/client'
import { Cart } from './cart'
import { Products, GET_ALL_PRODUCTS, GET_ALL_CURRENCIES } from './products'
import { addToCart, decrementCartItem, removeFromCart } from './common/utility'
import './common/app.scss'

const App = () => {
  const [isCartVisible, setCartVisibility] = useState(false)
  const [allProducts, setAllProducts] = useState()
  const [currency, setCurrency] = useState('USD')
  const { data: productData, refetch: refetchProductData } = useQuery(
    GET_ALL_PRODUCTS, { variables: { currency } }
  )
  const { data: currencyData } = useQuery(GET_ALL_CURRENCIES)

  useEffect(() => {
    if(allProducts && productData?.products) {
      const newProductState = productData?.products.map(product => ({
        ...allProducts.find(item => product.id === item.id),
        ...product
      }))
      setAllProducts(newProductState)
    }
    else {
      productData && setAllProducts(productData.products)
    }

  // adding allProducts state to the dependency array will cause
  // an ifinite loop
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [productData])

  return (
    <>
      <nav>
        <button className='cart-button button' onClick={() => {
          setCartVisibility(true)
        }}>
          <i className="fa fa-shopping-cart"></i>
        </button>
      </nav>
      <Products
        addToCart={addToCart}
        setAllProducts={setAllProducts}
        allProducts={allProducts}
        isCartVisible={isCartVisible}
        setCartVisibility={setCartVisibility}
        currency={currency}
      />
      {isCartVisible && <Cart
        allProducts={allProducts}
        setAllProducts={setAllProducts}
        addToCart={addToCart}
        decrementCartItem={decrementCartItem}
        removeFromCart={removeFromCart}
        setCartVisibility={setCartVisibility}
        currencyData={currencyData}
        refetchProductData={refetchProductData}
        currency={currency}
        setCurrency={setCurrency}
      />}
    </>
  )
}

export default App;
